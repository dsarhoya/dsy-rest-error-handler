//
//  DSYRestErrorHandlerTests.swift
//  DSYRestErrorHandlerTests
//
//  Created by Alfredo Luco on 20-03-18.
//  Copyright © 2018 dsarhoya. All rights reserved.
//

import XCTest
@testable import DSYRestErrorHandler

class DSYRestErrorHandlerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        class ErrorResponse{
            var error_code: String = ""
            var error_field: String = ""
            var error_message: String = ""
            
            init(errorCode: String, errorField: String, errorMessage: String) {
                self.error_code = errorCode
                self.error_field = errorField
                self.error_message = errorMessage
            }
        }
        let errorResponse1 = ErrorResponse(errorCode: "FIELD_I+,mnb", errorField: "first_name", errorMessage: "first name empty")
        
        let requiredFieldHandler = ErrorHandler(errorDefault: "Hay un campo requerido.", succesors: nil, closure: { error -> Bool in
            return (error as! ErrorResponse).error_code == "FIELD_REQUIRED"
        })
        
        let invalidFieldHandler = ErrorHandler(errorDefault: "Hay un campo inválido.", succesors: [
            ErrorHandler(errorDefault: "El nombre es inválido.", succesors: nil, closure: { error -> Bool in
                return (error as! ErrorResponse).error_field == "first_name"
            }),
            ], closure: { error -> Bool in
                return (error as! ErrorResponse).error_code == "FIELD_INVALID"
        })
        
        let handler = ErrorHandler(errorDefault: "Ocurrió un error", succesors: [requiredFieldHandler, invalidFieldHandler], closure: { error -> Bool in
            return true
        })
        
        print(handler.handleError(errorResponse: errorResponse1) == "Ocurrió un error")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
