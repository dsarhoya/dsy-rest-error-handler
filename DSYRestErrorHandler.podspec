#
#  Be sure to run `pod spec lint RestErrorHandler.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  #1.- metadata
  s.platform     = :ios
  s.ios.deployment_target = '8.0'
  s.name         = "DSYRestErrorHandler"
  s.summary      = "Handle errors messages of Restfull apis"
  s.requires_arc = true

  #2.- version
  s.version      = "0.0.1"

  # 3
  #s.licence = { :type => "MIT", :file => "LICENSE" }

  # 4
  s.author = { "dsarhoya " => "contacto@dsy.cl" }

  # 5
  s.homepage = "https://bitbucket.org/dsarhoya/dsy-rest-error-handler"

  # 6
  s.source = { :git => "git@bitbucket.org:dsarhoya/dsy-rest-error-handler.git", :tag => "0.0.1" }

  # 7
  s.ios.frameworks = 'UIKit', 'Foundation'

  # 8
  s.ios.source_files = 'Sources/**/*.swift'

  #9
  #s.ios.resource_bundle = {"DSYCustomEurekaRow" => 'Sources/Media.xcassets'}
  #s.resources = "Sources/**/*.xcassets"

end
