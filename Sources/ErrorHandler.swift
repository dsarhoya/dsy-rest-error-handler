//
//  ErrorHandler.swift
//  RestErrorHandler
//
//  Created by Alfredo Luco on 15-03-18.
//  Copyright © 2018 dsarhoya. All rights reserved.
//

import Foundation
public class ErrorHandler{
    
    var error_default: String?
    var succesors: [ErrorHandler]?
    var closure: ((_ error: Any) -> Bool)
    
    public init(errorDefault: String?,succesors: [ErrorHandler]?,closure: @escaping ((_ error: Any) -> Bool)) {
        self.error_default = errorDefault
        self.succesors = succesors
        self.closure = closure
    }
    
    public func handleError(errorResponse: Any)-> String? {
        if(!self.closure(errorResponse)){
            return nil
        }
        guard self.succesors != nil else {
            return self.error_default
        }
        for succesor in self.succesors! {
            if let errorStr = succesor.handleError(errorResponse: errorResponse){
                return errorStr
            }
        }
        return self.error_default
    }
    
    
}
